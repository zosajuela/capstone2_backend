const express = require('express');
const router = express.Router()
const Student = require('../model/model');

// create and save new student
module.exports.create = (req,res)=>{
    // validate requests
    if(!req.body){
        res.status(400).send({ message : "Content can not be emtpy!"});
        return;
    }

    // new student
    const student = new Student({
        name : req.body.name,
        lastname : req.body.lastname,
        email : req.body.email,
        mobile : req.body.mobile,
        gender: req.body.gender,
        status : req.body.status
    })

    // save student in the database
    student
        .save(student)
        .then(data => {
            //res.send(data)
            res.redirect('/add-student');
        })
        .catch(err =>{
            res.status(500).send({
                message : err.message || "Some error occurred while creating a create operation"
            });
        });

}

// retrieve and return all sttudent/ retrive and return a single student
module.exports.find = (req, res)=>{

    if(req.query.id){
        const id = req.query.id;

        Student.findById(id)
            .then(data =>{
                if(!data){
                    res.status(404).send({ message : "Not found student with id "+ id})
                }else{
                    res.send(data)
                }
            })
            .catch(err =>{
                res.status(500).send({ message: "Erro retrieving student with id " + id})
            })

    }else{
        Student.find()
            .then(student => {
                res.send(student)
            })
            .catch(err => {
                res.status(500).send({ message : err.message || "Error Occurred while retriving student information" })
            })
    }

    
}

// Update a new idetified student by student id
module.exports.update = (req, res)=>{
    if(!req.body){
        return res
            .status(400)
            .send({ message : "Data to update can not be empty"})
    }

    const id = req.params.id;
    Student.findByIdAndUpdate(id, req.body, { useFindAndModify: false})
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Update student with ${id}. Maybe student not found!`})
            }else{
                res.send(data)
            }
        })
        .catch(err =>{
            res.status(500).send({ message : "Error Update student information"})
        })
}

// Delete a student with specified student id in the request
module.exports.delete = (req, res)=>{
    const id = req.params.id;

    Student.findByIdAndDelete(id)
        .then(data => {
            if(!data){
                res.status(404).send({ message : `Cannot Delete with id ${id}. Maybe id is wrong`})
            }else{
                res.send({
                    message : "Student was deleted successfully!"
                })
            }
        })
        .catch(err =>{
            res.status(500).send({
                message: "Could not delete Student with id=" + id
            });
        });
}