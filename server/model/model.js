const mongoose = require('mongoose');

var studentSchema = new mongoose.Schema({
    name : {
        type : String,
        required: true
    },
    lastname : {
        type : String,
        required: true
    },
    email : {
        type: String,
        required: true,
        unique: true
    },
    mobile : {
        type: Number,
        required: true,
        unique: true
    },
    gender : String,
    status : String
})

module.exports = mongoose.model('Student', studentSchema)