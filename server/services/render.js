
const axios = require('axios');


module.exports.homeRoutes = (req, res) => {
    // Make a get request to /api/student
    axios.get('https://evening-fjord-91994.herokuapp.com/api/student')
        .then(function(response){
            res.render('index', { student : response.data });
        })
        .catch(err =>{
            res.send(err);
        })
 
}
//Note:after testing API in postman this was changed and axios was added
//exports.homeRoutes=(req,res)=>{
 //   res.render('index');
//} 

module.exports.add_student=(req,res)=>{
    res.render('add_student');
}

//this was change after testing front end sending files to mongo successfully
// exports.update_student=(req,res)=>{
//     res.render('update_student');
// }
module.exports.update_student = (req, res) =>{
    axios.get('https://evening-fjord-91994.herokuapp.com/api/student',{params:{id:req.query.id}})
        .then(function(studentdata){
            res.render("update_student",{student:studentdata.data})
        })
        .catch(err =>{
            res.send(err);
        })
}