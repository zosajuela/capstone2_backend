const mongoose = require('mongoose')//lets acquire the dependency needed to model our schema
//CREATE A MODEL FOR COURSE using the notes from 136-143

//lets describe the anatomy of our document. 
const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		required: [true, 'Course name is required']
	},
	description: {
		type: String,
		required: [true, 'Course description is required']
	}, 
	price: {
		type: Number,
		required: [true, 'Course price is required']
	}, 
	isActive: {
		type: Boolean,//Boolean
		default: true
	} , //this will describe if the course is still available for enrollment. 
	createdOn: {
		type: Date,
		default: new Date()
	},//this will describe "when" the subject was added in the system. 
	enrollees: [
	  {
	  	userId: {
	  	   type: String,
	  	   required: [true, 'user Id is required']//this will describe the id of the user to get the details.
	  	},
	  	enrolledOn: {
	  		type: Date,
	  		default: new Date()
	  	} //when the student enrolled for the subject. 
	  } 
	]//this will describe/display the list of students that are enrolled for a specific subject. 
}) 

module.exports = mongoose.model('Course', courseSchema)
