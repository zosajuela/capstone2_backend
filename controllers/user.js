const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth')
const Course = require('../models/course')

//we need to create a controller to check if the credentials in the email and password already exists within the database ...this means a user cannot register an email that is already registered in our application. 
//so now lets create a function to check if the email already exists 
// so first we have to get an input from the user in this case we need to get his/her email address
module.exports.emailExists = (params) => {
	//what will now happen in this function? 
	// upon getting the input data from the user now we have to run a search function/query in our database, take note that the input will be the email property of the user so we cannot use the findById method so instead we will just use the simple find() function. 
	//now once the query is able to find atleast 1 match then the registration should not be a success meaning the registration is false 
	return User.find({ email: params.email }).then(result => {
		return result.length > 0 ? true : false
	})
}

//this function is created to register a user in our application 
module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email, 
		mobileNo: params.mobileNo,
		password: bcrypt.hashSync(params.password, 10)
	})

	return user.save().then((user,err) => {
		return (err) ? false : true 
	})
} 

//the next function is to allow the registered user to login our app. 
module.exports.login = (params) => {
	//the first thing the program has to do to allow the user to log in is to search the data base if the user exists. 
	return User.findOne({ email: params.email }).then(user => {
		//so now, upon getting the email parameter of the user, the entered data in the email parameter has to pass the following requirements, so to do this we have to create a control structure 
		//the 1st requirement will be to check if the email is not empty
		if(user === null) { return false }
		//the 2nd requirement for the user to be able to log in will be to check if the password matches the password stored inside the database. 
		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
		if(isPasswordMatched) {
			//so this will happen if the data passed both the requirements needed to log in, if the they pass, this section of code will run.
			return { access: auth.createAccessToken(user.toObject()) }
		} else {
			//and if not this else branch will be the proper response
			return false 
		}
	})
} 

//lets create a function that will allow us to retrieve a user from our database.
//we want to be able to view a specific user of our choosing 
//this function requires a user input..why? ..well to propery identify which user you would ike to target
module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

module.exports.enroll = (params) => {
	return User.findById(params.userId).then(user => {
	  user.enrollments.push({ courseId: params.courseId })
	  //save the changes inside the database.
	  return user.save().then((user, err) => {
		//this next section will allow us to insert the user insde the Course collection to have a feature that will allow you to identify the students enrolled in a course.
		return Course.findById(params.courseId).then(course => {
		   course.enrollees.push({ userId: params.userId })
 
		   return course.save().then((course, err) => {
			   return (err) ? false : true 
		   })
		})
	  })
	})
 }
 
 

