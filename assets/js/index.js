


$("#add_student").submit(function(event){
    alert("Data Inserted Successfully!");
})


$("#update_student").submit(function(event){
    event.preventDefault();

    var unindexed_array = $(this).serializeArray();
    var data = {}

    $.map(unindexed_array, function(n, i){
        data[n['name']] = n['value']
    })


    var request = {
        "url" : `https://evening-fjord-91994.herokuapp.com/api/student/${data.id}`,
        "method" : "PUT",
        "data" : data
    }

    $.ajax(request).done(function(response){
        alert("Data Updated Successfully!");
    })

})

if(window.location.pathname == "/"){
    $ondelete = $(".table tbody td a.delete");
    $ondelete.click(function(){
        var id = $(this).attr("data-id")

        var request = {
            "url" : `https://evening-fjord-91994.herokuapp.com/api/student/${id}`,
            "method" : "DELETE"
        }

        if(confirm("Do you really want to delete this record?")){
            $.ajax(request).done(function(response){
                alert("Data Deleted Successfully!");
                location.reload();
            })
        }

    })
}


let numero = document.getElementById("numero");
let spa = document.getElementById("spa");
numero.addEventListener('keyup', (event) => {
    if(numero.value.length < 11 || numero.value === "") {
     spa.innerHTML = "please make sure to enter an 11 digit number";
     return false;
    }else{
        spa.innerHTML = "you've entered a valid number";
        return true;
    }
});
$("#numer").attr({
    "max" : 11,
    "min" : 11
 });

//     if(number.length != 10) {
//         window.alert("Phone number must be 10 digits.");
//         number.focus();
//         return false;
//     }
// });

